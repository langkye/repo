##! ~/Virtualenv/Python37/python/bin/python
# -*- coding:utf-8 -*-

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as Wait
import yaml
import os


def redeploy(repo_user_name, repo_name, login_user, login_pwd, oSystem):
    print("\nstart refresh gitee pages...")

    os_type = {
        '1': 'chromedriver_linux',
        '2': 'chromedriver_mac',
        '3': 'chromedriver_win.exe'
    }

    url = "https://gitee.com/" + repo_user_name + "/" + repo_name + "/pages"

    # path = os.path.dirname(os.path.realpath(__file__))
    # driver_path = "script/{}".format(os_type[oSystem])
    driver = os.path.abspath("redeploy_gitee_page/script/chromedriver_linux")
    chrome_options = Options()
    chrome_options.add_argument("--window-size=1920,1080")
    chrome_options.add_argument("--start-maximized")
    chrome_options.add_argument("--headless")
    browser = webdriver.Chrome(executable_path=driver, options=chrome_options)

    browser.get(url)

    Wait(browser, 10).until(EC.presence_of_element_located((By.CLASS_NAME, "item.git-nav-user__login-item")))
    print("load finish. url=" + url)
    login_btn = browser.find_element_by_class_name("item.git-nav-user__login-item")
    login_btn.click()

    Wait(browser, 10).until(EC.presence_of_element_located((By.ID, "user_login")))
    Wait(browser, 10).until(EC.presence_of_element_located((By.ID, "user_password")))
    print("login page load finish.")
    user_input = browser.find_element_by_id("user_login")
    pwd_input = browser.find_element_by_id("user_password")
    login_btn = browser.find_element_by_name("commit")
    user_input.send_keys(login_user)
    pwd_input.send_keys(login_pwd)
    login_btn.click()

    Wait(browser, 10).until(
        EC.presence_of_element_located((By.CLASS_NAME, "button.orange.redeploy-button.ui.update_deploy")))
    print("login finish.")
    deploy_btn = browser.find_element_by_class_name('button.orange.redeploy-button.ui.update_deploy')

    browser.execute_script("window.scrollTo(100, document.body.scrollHeight);")
    deploy_btn.click()
    dialog = browser.switch_to.alert
    dialog.accept()
    print("refresh gitee pages finish.")
    browser.close()


def input_required():
    repo_user_name = input("仓库用户名称:\n>>>")
    if len(repo_user_name) == 0:
        print("输入不能为空!请重新输入")
        input_required()

    repo_name = input("仓库名称:\n>>>")
    if len(repo_name) == 0:
        print("输入不能为空!请重新输入")
        input_required()

    login_user = input("登录用户名称:\n>>>")
    if len(login_user) == 0:
        print("输入不能为空!请重新输入")
        input_required()

    login_pwd = input("登录密码:\n>>>")
    if len(login_pwd) == 0:
        print("输入不能为空!请重新输入")
        input_required()

    oSystem = input("当前操作系统(默认Linux)\n\t**< 1:Linux | 2:Mac | 3:Windows >**\n>>>")
    if len(os) == 0:
        redeploy(repo_user_name, repo_name, login_user, login_pwd, "1")

    redeploy(repo_user_name, repo_name, login_user, login_pwd, oSystem)


def reade_conf():
    path = os.path.dirname(os.path.realpath(__file__))
    config = os.path.join(path, "config.yaml")

    f = open(config)  # 打开yaml文件

    # d = yaml.load(f) ##yaml5.1之前的版本：使用load方法加载
    d = yaml.load(f, Loader=yaml.FullLoader)  ##yaml5.1之后的版本:使用load方法加载

    repo_user_name = d['repo_user_name']
    repo_name = d['repo_name']
    login_user = d['login_user']
    login_pwd = d['login_pwd']
    oSystem = str(d['os'])

    redeploy(repo_user_name, repo_name, login_user, login_pwd, oSystem)


if __name__ == '__main__':
    # 方式一:通过键盘输入必要信息
    # input_required()

    # 方式二:通过config.yaml配置文件
    reade_conf()
