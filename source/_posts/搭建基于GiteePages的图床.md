---
title: 搭建基于GiteePages的图床
date: 2020-12-30 15:52:07
description:
tags: 
  - 图床
categories: 
  - 博客
keywords:
  - 图床
  - Gitee
top_img:
cover:
comments: true
toc:
toc_number: 1
copyright: true
copyright_author: Langkye
copyright_author_href: https://www.cnblogs.com/langkyeSir
copyright_url: https://www.cnblogs.com/langkyeSir
copyright_info: 原创<b>文章</b>
highlight_shrink: true
aside: true
---

### 1、新建仓库

> 在Gitee中创建一个仓库用于存储图片。

![](https://gitee.com/langkye/imagesBed/raw/master/gitee_blog/img/20201230154444.png)

### 2、初始化本地库

> 在本地创建目录并初始化

```bash
git init
git remote add origin https://gitee.com/langkye/imageBed.git
```

### 3、上传文件

e.g. 我在本地创建`gitee_blog`目录，并添加了一个文件`favicon.ico`。

```bash
git add .
git commit -m 'upload favicon'
git push -u origin master
```

### 4、开启`Gitee Pages`服务

1. 点击**服务**，选择`Gitee Pages`。

   ![](https://gitee.com/langkye/imagesBed/raw/master/gitee_blog/img/20201230154543.png)

2. 部署

   ![](https://gitee.com/langkye/imagesBed/raw/master/gitee_blog/img/20201230154506.png)

3. 访问上面网址。

   > 记得加上自己资源路径

   ![](https://gitee.com/langkye/imagesBed/raw/master/gitee_blog/img/20201230154532.png)

### Gitee图床工具
1. PicUploader
   > PicUploader 是一个用php编写的图床工具，它能帮助你快速上传你的图片到云图床，并自动返回Markdown格式链接到剪贴板。

   适合人群：有编程与 Git 经验的开发者

   项目地址: https://gitee.com/xiebruce/PicUploader

   特点：支持Mac/Win/Linux服务器、支持压缩后上传、添加图片或文字水印、多文件同时上传、同时上传到多个云、右击任意文件上传、快捷键上传剪贴板截图、Web版上传、支持作为Mweb/Typora发布图片接口、作为PicGo/ShareX/uPic等的自定义图床，支持在服务器上部署作为图床接口，支持上传任意格式文件。

2. PicGo
   > PicGo官网地址: https://molunerfinn.com/PicGo/
   
   > 项目地址: https://github.com/Molunerfinn/PicGo
   
   支持Windows、MacOS 和 Linux

   软件目前覆盖的图床有8个平台: SM.MS图床、腾讯云COS、微博图床、GitHub图床、七牛图床、Imgur图床、阿里云OSS、又拍云图床。

3. uPic
   > uPic(upload Picture) 是一款 Mac 端的图床(文件)上传客户端。

   - 适合人群：没有编程与 Git 经验的小白用户

   - 项目地址：https://gitee.com/gee1k/uPic

   特点： 无论是本地文件、或者屏幕截图都可自动上传，菜单栏显示实时上传进度。上传完成后文件链接自动复制到剪切板，让你无论是在写博客、灌水聊天都能快速插入图片。 连接格式可以是普通 URL、HTML 或者 Markdown，仍由你掌控。

### 参考文章
*1. [PicGo：免费搭建个人图床](https://zhuanlan.zhihu.com/p/128014135)*

*2. [图床太难找？有了Gitee上这两个图床工具，小白大佬都能轻松搞定](https://zhuanlan.zhihu.com/p/138938782)*

*3. [PicGo + Gitee(码云)实现markdown图床](https://www.jianshu.com/p/b69950a49ae2)*
