---
title: 使用Python脚本对GiteePages进行一键部署
date: 2020-12-31 10:23:36
tags:
  - GiteePages
  - Python
  - 自动化
categories:
  - 博客 
  - Python
keywords:
  - 部署
  - Gitee
top_img:
cover:
comments: true
toc:
toc_number: 2
---
**本次系统环境**
> os: Deepin（Linux）
> Python: 3.7
> lib: PyYAML==5.3.1 | selenium==3.141.0
> extend_driver: chromedriver

# 使用说明
## 参数
Python脚本和config.yaml配置文件中包含的参数信息
```markdown
repo_user_name = "Gitee用户名"
repo_name = "仓库名"
login_user = "登录用户"
login_pwd = "登录密码"
os = "输入数值1~~3(你的操作系统:1 Linux | 2 Mac | 3 Windows)"
```


## 驱动
> 本文使用的是 `Linux 87.0.4280.88（正式版本） （64 位）`

你需要根据你的操作系统和谷歌浏览器选择对应的驱动。如果你的浏览器版本与我的一致,那么恭喜你^_^,直接拿去用吧~我已经把Linux、Mac和Windows的驱动准备好了。

如果你的谷歌浏览器版本与我的不一致也不要紧,下面我会告诉你如何下载与配置。

1. 查看你的chrome版本
   
   在chrome浏览器地址栏输入: chrome://version/ 查看自己的版本信息。
   
   然后你将看到:
   ![chrome版本](https://gitee.com/langkye/imagesBed/raw/master/gitee_blog/img/20201230180937.png)

2. 下载驱动
   
   选择自己的驱动 Windows|Linux|Mac。 驱动下载: [地址](http://npm.taobao.org/mirrors/chromedriver/)


3. 重要提示: 需要把驱动放到下面两个路径下
   
   1.谷歌的安装路径
   
   2.python的安装路径

# Python环境
> 为了避免干扰,我使用虚拟环境来进行管理。
> 
> 当然你使用安装系统安装的环境也没关系。
## 使用安装环境
1. 安装依赖库
   
   在`requirements.txt`所在目录执行:
   ```bash
   pip install -r requirements.txt
   ```

## 创建寻环境
1. 创建目录, 并进入该目录
    ```bash
    mkdir -p ~/Virtualenv/Python37 && cd ~/Virtualenv/Python37
    ```
2. 创建虚拟环境
    ```bash
    python3 -m venv python
    ```
   如果报错:
    ```markdown
    The virtual environment was not created successfully because ensurepip is not
    available
    ```
   执行:
    ```bash
    apt-get install python3-venv
    ```
3. 切换到虚拟环境
> 如果使用IDE,直接将脚本切换为虚拟环境的可执行程序。
> 如果使用命令行,可以直接切换到寻虚拟环境所在的目录,直接执行即可;或者执行虚拟环境中的`active`。(Windows 脚本在Script下| Linux、Mac在bin下)
> e.g. 对于Linux: source ./bin/activate

4. 安装该本脚本需要的依赖包
```bash
pip install -r requirements.txt
```

## 运行脚本
1. 准备你的仓库信息
   我提供了通过键盘输入和配置文件两种方式来获取你的仓库和密码等信息。

   默认是读取配置文件的方式进行获取。如果你要切换,你可以在`bin.py`的文件末尾进行配置,我做了说明。

2. 执行脚本 
   如果你已经了解并准备好了,那么直接执行它吧~
   ```bash
   python bin.py
   ```

# 附录
脚本|驱动|配置文件等信息,到我的Gitee上下载-><a href="https://gitee.com/langkye/repo/tree/master/redeploy_gitee_page" target="_blank">地址</a>

1. 配置文件
```yaml
repo_user_name: 仓库用户
repo_name: 仓库名
login_user: 登录名
login_pwd: 登录密码
os: 1
```

2. Python脚本
   ```python
   ##! ~/Virtualenv/Python37/python/bin/python
   # -*- coding:utf-8 -*-
   
   from selenium import webdriver
   from selenium.webdriver.chrome.options import Options
   from selenium.webdriver.common.by import By
   from selenium.webdriver.support import expected_conditions as EC
   from selenium.webdriver.support.ui import WebDriverWait as Wait
   import yaml
   import os
   
   
   def redeploy(repo_user_name, repo_name, login_user, login_pwd, oSystem):
       print("\nstart refresh gitee pages...")
   
       os_type = {
           '1': 'chromedriver_linux',
           '2': 'chromedriver_mac',
           '3': 'chromedriver_win.exe'
       }
   
       url = "https://gitee.com/" + repo_user_name + "/" + repo_name + "/pages"
   
       # path = os.path.dirname(os.path.realpath(__file__))
       # driver_path = "script/{}".format(os_type[oSystem])
       driver = os.path.abspath("script/chromedriver_linux")
       chrome_options = Options()
       chrome_options.add_argument("--window-size=1920,1080")
       chrome_options.add_argument("--start-maximized")
       chrome_options.add_argument("--headless")
       browser = webdriver.Chrome(executable_path=driver, options=chrome_options)
   
       browser.get(url)
   
       Wait(browser, 10).until(EC.presence_of_element_located((By.CLASS_NAME, "item.git-nav-user__login-item")))
       print("load finish. url=" + url)
       login_btn = browser.find_element_by_class_name("item.git-nav-user__login-item")
       login_btn.click()
   
       Wait(browser, 10).until(EC.presence_of_element_located((By.ID, "user_login")))
       Wait(browser, 10).until(EC.presence_of_element_located((By.ID, "user_password")))
       print("login page load finish.")
       user_input = browser.find_element_by_id("user_login")
       pwd_input = browser.find_element_by_id("user_password")
       login_btn = browser.find_element_by_name("commit")
       user_input.send_keys(login_user)
       pwd_input.send_keys(login_pwd)
       login_btn.click()
   
       Wait(browser, 10).until(
           EC.presence_of_element_located((By.CLASS_NAME, "button.orange.redeploy-button.ui.update_deploy")))
       print("login finish.")
       deploy_btn = browser.find_element_by_class_name('button.orange.redeploy-button.ui.update_deploy')
   
       browser.execute_script("window.scrollTo(100, document.body.scrollHeight);")
       deploy_btn.click()
       dialog = browser.switch_to.alert
       dialog.accept()
       print("refresh gitee pages finish.")
       browser.close()
   
   
   def input_required():
       repo_user_name = input("仓库用户名称:\n>>>")
       if len(repo_user_name) == 0:
           print("输入不能为空!请重新输入")
           input_required()
   
       repo_name = input("仓库名称:\n>>>")
       if len(repo_name) == 0:
           print("输入不能为空!请重新输入")
           input_required()
   
       login_user = input("登录用户名称:\n>>>")
       if len(login_user) == 0:
           print("输入不能为空!请重新输入")
           input_required()
   
       login_pwd = input("登录密码:\n>>>")
       if len(login_pwd) == 0:
           print("输入不能为空!请重新输入")
           input_required()
   
       oSystem = input("当前操作系统(默认Linux)\n\t**< 1:Linux | 2:Mac | 3:Windows >**\n>>>")
       if len(os) == 0:
           redeploy(repo_user_name, repo_name, login_user, login_pwd, "1")
   
       redeploy(repo_user_name, repo_name, login_user, login_pwd, oSystem)
   
   
   def reade_conf():
       path = os.path.dirname(os.path.realpath(__file__))
       config = os.path.join(path, "config.yaml")
   
       f = open(config)  # 打开yaml文件
   
       # d = yaml.load(f) ##yaml5.1之前的版本：使用load方法加载
       d = yaml.load(f, Loader=yaml.FullLoader)  ##yaml5.1之后的版本:使用load方法加载
   
       repo_user_name = d['repo_user_name']
       repo_name = d['repo_name']
       login_user = d['login_user']
       login_pwd = d['login_pwd']
       oSystem = str(d['os'])
   
       redeploy(repo_user_name, repo_name, login_user, login_pwd, oSystem)
   
   
   if __name__ == '__main__':
       # 方式一:通过键盘输入必要信息
       # input_required()
   
       # 方式二:通过config.yaml配置文件
       reade_conf()
   ```

# 参考文章
1. https://www.jianshu.com/p/19cc4eb0f199
2. https://www.cnblogs.com/wulixia/p/11200023.html